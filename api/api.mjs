/**
 * 
 * The user facing API for programming envisense devices and adding new ones to chirpstack and the SQL database (which stores the programs/schedules)
 * 
 * Created by Reese Grimsley, July 2021
 */


import * as cs from './es_chirpstack.mjs'
import * as sql from './es_sql.mjs'
import * as schedule from './es_schedule.mjs'
// require('./es_chirpstack.mjs')


import { createRequire } from "module"; // in classic JS form, using 'require' and 'import in the same file/module is a no-no, and in classic-er form, there is (probably unsafe) workaround. Asinine.
const require = createRequire(import.meta.url);
const config = require("./config.json");


/**
 * Create a full clone of an object, maintaining the class and all instance variables
 *
 * @param obj any javascript object, including an instance of a class
 * @returns A full deep copy of the input object
 */
function clone(obj) {
    return Object.create(
        Object.getPrototypeOf(obj), 
        Object.getOwnPropertyDescriptors(obj) 
    );
}

/**
 * Insert a new device into the EnviSense system by creating the device in chirpstack and the SQL database (which store LoRaWAN configurations and device schedules, respectively).
 *   Chirpstack requires additional information like an application ID, network/application key, and device profile. Each of these are retrieved from a config.json file in the same directory, as well as the host, port, username, and password used to authenticate requests to chirpstack and SQL
 * 
 * @param devEUI device EUI, which is a 16-character hexadecimal string  used to uniquely identify a device within Chirpstack
 * @param deviceName The descriptive name of a device (a string)
 * @param useSDI12 A boolean indicating whether this is an EnviSense that uses SDI12 transducers or not. This is used to select which default schedules to add when creating the device
 * @returns A promise that will return a JSON object with boolean values to indicate if adding the device to chirpstack and SQL succeeded, and if each of the schedules was written correctly (6 keys and booleans, total)
 */
export async function createNewDevice(devEUI, deviceName, useSDI12)
{
    console.log('CreateNewDevices with name ' + deviceName + ' and devEUI ' + devEUI)
    if (useSDI12 == undefined) useSDI12 = false

    //create the SQL client using information from the config file, which we import above.
    let sqlClient = new sql.ES_sql(config.mysql)

    var results = {'chirpstack':false, 'SqlCreate':false, 'scheduleA':false, 'scheduleB':false, 'scheduleC':false, 'scheduleD':false}
    console.log(results)

    // generate a set of schedules that we will write after adding the new device to Chirpstack and SQL 
    if (useSDI12)
    {
        var scheduleA = schedule.generateDefaultSDI12Schedule()
        var scheduleB = schedule.generateHourlySDI12SamplingSchedule()
        var scheduleC = schedule.generate6HourlySDI12SamplingSchedule()
        //we'll leave schedule D to be the regular default schedule
        var scheduleD = schedule.generateDefaultSDI12Schedule()
    }
    else 
    {
        var scheduleA = schedule.generateDefaultSchedule()
        var scheduleB = schedule.generateHourlySamplingSchedule()
        var scheduleC = schedule.generate6HourlySamplingSchedule()
                //we'll leave schedule D to be the regular default schedule

        var scheduleD = schedule.generateDefaultSchedule()
    }

    //we'll chain a bunch of promises together
    // add device to chirpstack
    let addDevicePromise = cs.addNewDevice(devEUI, deviceName) //returns a promise, which we attach a callback to (.then(result => callback_function))
    .then(result => {
        console.log('addNewDevice to chirpstack returned')
        results.chirpstack = result
        console.log(results)
        if (results.chirpstack === true)
        {
            console.log('Added device to Chirpstack')
            return sqlClient.insertNewDevice(devEUI, deviceName) //this also returns a promise, so we'll 'thenify' that too with callbacks
        } 
        else return false //returning false will propagate through, such that the rest of the members of the ressults object will be false
    }).catch(error => { //all promises with .then need .catch(result=>callback) as well
        console.error(error)
        console.log('failed chirpstack add')
        results.chirpstack = false
        return false //propagate the failure through the rest of the promises
    })
    .then(result => {   //respond to the promise to add device to SQL database
        console.log(result)
        results.SqlCreate = result
        if (results.SqlCreate === true)
        {
            console.log('Added device to SQL database')
            //prep and write schedule A
            scheduleA.setScheduleIDs('A')
            scheduleA.setSequenceNumbers(1)
            return sqlClient.writeNewSchedule(devEUI, scheduleA) //returns a promise, which we'll thenify
        }
        else 
        {
            console.log('Failed to add device to SQL database')
            return false //propagate failure
        }
    }).catch(error => {
        console.error(error)
        console.log('failed SQL device add')
        results.sqlCreate = false
        return false
    })
    .then(result => {   //respond to the promise to add default schedule A
        results.scheduleA = result
        if (results.scheduleA === true)
        {
            console.log('Added schedule A to database')
            //prep and write schedule B
            scheduleB.setScheduleIDs('B')
            scheduleB.setSequenceNumbers(1)
            return sqlClient.writeNewSchedule(devEUI, scheduleB) //return a promise to write the next schedule
        }
        else return false
    }).catch(error => {
        console.error(error)
        console.log('Failed schedule A write')
        results.scheduleA = false
        return false
    })
    .then(result => {   //respond to the promise to add default schedule B
        results.scheduleB = result
        if (results.scheduleB === true)
        {
            console.log('Added schedule B to database')
            //prep and write schedule C
            scheduleC.setScheduleIDs('C')
            scheduleC.setSequenceNumbers(1)
            return sqlClient.writeNewSchedule(devEUI, scheduleC)
        }
        else return false
    }).catch(error => {
        console.error(error)
        console.log('Failed schedule B write')
        results.scheduleB = false
        return false
    })
    .then(result => {   //respond to the promise to add default schedule C
        results.scheduleC = result
        if (results.scheduleC === true)
        {
            console.log('Added schedule C to database')
            //prep and write schedule D
            scheduleD.setScheduleIDs('D')
            scheduleD.setSequenceNumbers(1)
            return sqlClient.writeNewSchedule(devEUI, scheduleD)
        }
        else return false
    }).catch(error => {
        console.error(error)
        console.log('Failed schedule C write')
        results.scheduleC = false
        return false
    })
    .then(result => {   //respond to the promise to add default schedule D
        results.scheduleD = result
        if (results.scheduleD === true)
        {
            console.log('Added schedule D to database')
        }
        return results //we'll finally return the whole results object that we've been filling in with each request
    }).catch(error => {
        console.error(error)
        console.log('Failed schedule D write')
        results.scheduleD = false
        return results //we'll finally return the whole results object that we've been filling in with each request
    })

    return addDevicePromise 
}

/**
 * Read a the most recent device schedule for a given device and schedule ID
 * 
 * @param devEUI The unique device identifer (16-character hex string)
 * @param scheduleID The schedule ID we want to query the whole schedule (24 blocks) for. We'll return whichever is newest according to the insertion timestamp
 * @returns a promise for an schedule.ES_schedule containing the schedule blocks for this device. If this fails, it will return a null or false value
 */
export async function readDeviceSchedule(devEUI, scheduleID)
{
    let sql_client = new sql.ES_sql(config.mysql)

    return sql_client.getDeviceSchedule(devEUI, scheduleID)

}

/**
 * Read the most recent device schedule block for a given device, schedule ID, and hour
 * 
 * @param devEUI The unique device identifer (16-character hex string)
 * @param scheduleID The schedule ID we want to query the whole schedule (24 blocks) for. We'll return whichever is newest according to the insertion timestamp
 * @param hour The hour of the schedule block to retrieve with the query
 * @returns a promise for an schedule.ES_schedule_block containing the set of instructions to be run on the given device for the specified hour when the specified scheduleID is active. If this fails, it will return a null or false value
 */
export async function readDeviceScheduleHour(devEUI, scheduleID, hour)
{
    // this is effectively a wrapper for the sql_client's nearly identical function
    let sql_client = new sql.ES_sql(config.mysql)

    return sql_client.getDeviceScheduleForHour(devEUI, hour, scheduleID)
}

/**
 * Write a schedule block (type ES_schedule_block) for all the hours specified for each of the provided devices for the provided scheduleID
 * 
 * @param devices An array of DevEUIs, which are each 16-character hex strings that uniquely identify the LoRaWAN device within a Chirpstack application
 * @param hours An array of hours to program the device for. Each value should be a number between 0 and 23 inclusive
 * @param scheduleID The schedule identifer (charater A, B, C, or D) to write this for
 * @param base_schedule_block An ES_schedule_block that contains the set of instructions each schedule block written should contain. This will be copied so the sequence number, hour, and device can be updated
 * @param sequence_number Optional sequence number (a form of versioning). Must be a uint8
 * @returns A promise returning an array of boolean indicating whether each schedule write succeeded or not
 */
export async function writeScheduleBlocksToDevices(devices, hours, scheduleID, base_schedule_block, sequence_number)
{
    let sql_client = new sql.ES_sql(config.mysql)
    sql_client.connect()
    let promises = []
    
    for (let i = 0; i < devices.length; i++ )
    {
        let device = devices[i]
        // let schedule_block = JSON.parse(JSON.stringify(base_schedule_block))
        let schedule_block = clone(base_schedule_block)
        // console.log(schedule_block)
        schedule_block.setScheduleID(scheduleID)
        schedule_block.setDevEUI(device)

        //also need to get the sequence numbers, if not present
        if (sequence_number === undefined) 
        {
            // sql
            var seqNoPromise = sql_client.getMostRecentSequenceNumbers(device, scheduleID)
            .then(sequence_numbers => {
                let seqNo = Math.max.apply(Math, sequence_numbers)
                // console.log(seqNo)
                return seqNo
            }).catch(error => {
                console.error(error)
            })
            sequence_number = await seqNoPromise //maybe ineffient to await here. It's awkward because we only make this request if 'sequence_number' wasn't supplied
            sequence_number = (sequence_number + 1) % 256
            // console.log('sequence number ' + sequence_number)
        }
        schedule_block.setSequenceNumber(sequence_number)

        for (let h = 0; h < hours.length; h++)
        {
            let hour = hours[h]
            let schedule_block_to_write = clone(schedule_block)
            schedule_block_to_write.setHour(hour)
            //TODO: test
            // promises.push(writeDeviceScheduleBlock(schedule_block_to_write))
            promises.push(sql_client.writeScheduleBlock(schedule_block_to_write.devEUI, schedule_block_to_write))
        }
    }

    // collect all the promises and return the aggregate promise, which contains an array of true-false values indicating pass or failure to write the schedule. We also need to disconnect the SQL client here.
    let final_promise = Promise.all(promises)
    .then(result =>
    {
        sql_client.disconnect()
        return result  
    }).catch(error => {
        sql_client.disconnect()
        console.error(error)
    })

    return final_promise
}

/**
 * Convert the set of instructions from the form within the webpage (HTML input elements) into format more consistent with the underlying code's expectations
 * 
 * @param instructions The set of instructions pulled from the form fields in the web page, which are not yet of type ES_schedule_block
 * @returns an ES_schedule_block containing this set of basic instructions. Uses a default hour of 0, and the devEUI and sequence numbers are not populated
 */
export function convertFormOutputToScheduleBlock(instructions)
{
    let schedule_instructions = []
    for (let i = 0; i < instructions.length; i++)
    {
        let form_instr = instructions[i]
        let schedule_instr = new schedule.ES_schedule_instruction(form_instr.seconds, form_instr.instruction, form_instr.parameter)
        schedule_instructions.push(schedule_instr)
    }

    let s =  new schedule.ES_schedule_block(schedule_instructions, 0)
    return s
}

/**
 *  Wrapper to get the set of devices in chirpstack. It will use the application specified in the config.json file. 
 * @returns A promise for a list of device names and DevEUIs. If it fails, it will return whatever error chirpstack produces. 
 * 
 */
export async function getCSDevices()
{
    return cs.getDevices()
}

/**
 * Query for the set of instructions the SQL database knows about, which are meant ot be in a table called 'functions', which map the instruction/function name (a string) to the 1-byte identifer that will be used in the downlink message to the device (which must be consistent with the EnviSense firmware). Only the names of the instructions are returned in this promise, as the lorawan app-server will translate strings to bytes on its own.
 * @returns 
 * 
 */
export async function getInstructions()
{
    let client = new sql.ES_sql(config.mysql)
    return  client.getSchedulerInstructions()
}

/**
 *  Update the 'pending' schedule for the set of devices to the provided schedule ID. Each EnviSense has an entry in the SQL database designating the schedule that it should be using (indicated by a simple identifer A, B, C or D). When the device requests schedules from the server, it will respond with those that match this pending scheduleID
 * @param devices An array of device EUIs (16-charater hex strings) that designate which devices to update the pending schedule ID for
 * @param scheduleID A single character identifier A, B, C, or D to write into the pendingScheduleID field in the SQL database
 * @returns A promise for an array of booleans designating success or failure to update the pending ID
 */
export async function updatePendingScheduleForDevices(devices, scheduleID)
{
    let sql_client = new sql.ES_sql(config.mysql)
    sql_client.connect()

    let promises = []

    devices.forEach(devEUI => { // we could be more efficient if we did them altogether, but then one invalid devEUI would cause all to fail
        promises.push( sql_client.updatePendingScheduleID(devEUI, scheduleID)) //return a promise for each device. 
    });

    let final_promise = Promise.all(promises)
    .then(result =>
    {
        console.log('updatePendingSchedule returned: ' + result)
        sql_client.disconnect()
        return result  
    }).catch(error => {
        sql_client.disconnect()
        console.error(error)
    })

    return final_promise
}
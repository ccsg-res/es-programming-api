/**
 * 
 * A set of constants used within the EnviSense scheduler-programming API. 
 * 
 * Created by Reese Grimsley, July 2021
 */
export const NUM_SCHEDULE_HOURS = 24
export const MAX_SCHEDULE_TIME = 3599
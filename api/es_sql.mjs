/**
 * 
 * The SQL-handling APIs for programming and creating EnviSense devices. 
 * 
 * A SQL database maintains state variables and sampling schedules for EnviSense devices, and these functions will connect to said database, create SQL queries, and return results (generally as promises)
 * 
 * Created by Reese Grimsley, July 2021
 */

import dateformat from 'dateformat'

import * as mysql from 'mysql'
import * as mysql2 from 'mysql2/promise'
import * as k from './es_constants.mjs'
import * as schedule from './es_schedule.mjs'


/**Design pattern for query
 * 
        let sqlPromise = this.query(sqlPendingScheduleUpdate)
        .then(result => {
            console.log(result)
        }).catch(error => {
            console.error(error)
        })
 */

/**
 * A class that will connect to the SQL database, produce queries, respond with results, and maintain that connection until disconnected.
 */
export class ES_sql {
    //shamelessly copied from our envisense app-server codebase
    constructor(config) {
        this.connection = null
        if (config.socketPath)
        {
            // console.log("Using socketPath for MySQL communication");
            this.mySqlConfig = {
                socketPath 	: config.socketPath,
                user     	: config.user,  
                password 	: config.password,
                database 	: config.database,
                multipleStatements: true
            }
        }  
        else if (config.host)
        {
            // console.log("Using host for MySQL communication");
            this.mySqlConfig = {
                host 		: config.host,
                port		: 3306,
                user     	: config.user,  
                password 	: config.password,
                database 	: config.database,
                multipleStatements: true
            };
        }
        else
        {
            throw new Error('[es_mysql] ERROR (1): either a sockPath or host attribute is required in the config.json file');
        }
    }   
    /**
     * Use the stored configuration to connect to the database. If the connection was already established, disconnect and reconnect.
     * 
     * @returns Nothing, but will signal error if it occurs
     */
    connect() {
        try {
            if (this.connection)
            {
                console.log("[es_mysql] WARNING: attempt to establish a connection when one already exists");
                this.disconnect();
            }
            this.connection = mysql.createConnection( this.mySqlConfig );
            // console.log("[es_mysql] Connect succeeded");
        }
        catch (error) {
            console.trace("[es_mysql] ERROR (2) Connect failed: %j", error);
            this.disconnect();
            // throw(error);
        }
    }

    /**
     * Make a SQL query and return the results (within a promise, which can be 'await'd or 'then'ified)
     * 
     * @param sql The SQL query string. 
     * @returns A promise, which returns the rows within the result of the SQl transaction. If the transaction fails, it should produce a mildly informative error message
     */
    // Returns a promise that requires thenification
    query(sql) {
        // this.connect();
        return new Promise( ( resolve, reject ) => {
            // connection.query( sql, {}, ( err, rows ) => {
            this.connection.query( sql, ( err, rows, fields ) => {
                if ( err )
                {
                    this.disconnect();
                    return reject( err );
                }
                // console.log("ROWS: %j", rows);
                // console.log("FIELDS: %j", fields);
                resolve( rows );
            } );
        } );
    }

    /**
     * Tear down the SQL client connection
     * @returns Nothing; undefined. Signals error if this teardown fails. This is a no-operation if the connection is not active
     */
    disconnect() {
        if (this.connection)
        {
            this.connection.end( function(err) {
                // The connection is terminated now
                if (err)
                    console.log("[es_mysql] ERROR (3) Connection ended with error indication %j", err);
            });
            this.connection = null;
        }
    }

    /**
     * Get the set of instructions within the 'functions' table of the database, which maps an instruction name to its one byte identifier
     * @returns A promise for a JSON object, in which the keys are the function names and the values are the one-byte function identifiers
     */
    async getSchedulerInstructions() 
    {
        let sqlGetInstructions = 'SELECT * FROM `functions`'

        this.connect()
        let sqlPromise = this.query(sqlGetInstructions)
        .then(result => {
        
            let instructions = {}
            result.forEach(instr => {
                instr = Object.assign({}, instr) //convert sql RowDataPacket into JSON 
                instructions[instr.function_name] = instr.function_id
                // instr_obj[instr.function_name] = instr.function_id
                // instructions_array.push(instr_obj)
            });

            this.disconnect()
            // let instr_names = []; 
            // for (let i in instructions_array) {instr_names.push(Object.keys(instructions_array[i])[0])}
            return instructions
        }).catch(error => {
            console.error(error)
            this.disconnect()
        })

        return sqlPromise

    }

    /**
     * Create a new device within the SQL database by creating entries in the 'deployment' table (contains a device name, a deployment time, and an offset value to help convert non-contact distance (from ultrasonic sensor) to stream height) and 'device' (tracks current and pending schedule version) tables. By default, the EnviSense device will be set to use schedule 'A' (held in the 'device' table)
     * 
     * @param newDeviceDevEui The Device EUI, a 16-character hex string, which must be the same as what is in Chirpstack
     * @param newDeviceName The new of the device (a string) which must be the same as what is in Chirpstack
     * @param deploymentTime The time of deployment; defaults to the current time. Format should be 'yyyy-mm-dd hh:mm:ss'
     * @param heightOffsetCm distance from the ultrasonic sensor to the stream bed to help calculate water height. Defaults to 0.
     * @returns A promise that resolves to true or false, depending on whether the device was inserted into the database successfully or not.
     */
    async insertNewDevice(newDeviceDevEui, newDeviceName, deploymentTime, heightOffsetCm)
    {
        //set some default values if they aren't provided
        if (deploymentTime === undefined) {deploymentTime = dateformat(new Date(), 'yyyy-mm-dd hh:mm:ss')}
        if (heightOffsetCm === undefined) {heightOffsetCm = 0}

        newDeviceDevEui = newDeviceDevEui.toUpperCase()
        
        // Create query string to check tables; we want to avoid duplicates
        let sqlDeviceCheckDeployment = "SELECT `deviceName` FROM `deployment`" + 
                " WHERE deviceName=\"" + newDeviceName + "\";" 
        let sqlDeviceCheckDevice = "SELECT `devEUI` FROM `device` WHERE devEUI=\"" + newDeviceDevEui + "\"; "

        // Create query string to insert new device into proper tables 
        //  Deployment table query
        let sqlDeviceInsertDeployment = "INSERT INTO `deployment` (`time`, `deviceName`, `heightCM`) " + 
                "VALUES (\"" + deploymentTime + "\", \"" + newDeviceName + "\", " + heightOffsetCm +"); "

        // console.log(sqlDeviceInsertDeployment)
        //  Device table query
        let sqlDeviceInsertDevice = "INSERT INTO `device` " + 
                "(`devEUI`, `last_login`, `pendingScheduleID`, `currentScheduleID`"
        for (let i=0; i<24; i++)
        {
            var paddedI = "0" + i;
            var key = "seqFor" + paddedI.substr(paddedI.length-2);
            sqlDeviceInsertDevice += ", `" + key + "`"
        }
        sqlDeviceInsertDevice += ") VALUES (\"" + newDeviceDevEui + "\", \"" + deploymentTime + "\", " +
                "\"A\", \"A\""
        for (let i=0; i<24; i++)
        {
            sqlDeviceInsertDevice += ", " + 0
        }
        sqlDeviceInsertDevice += "); "
        
        // console.log(sqlDeviceInsertDevice)
        // console.log(sqlDeviceCheckDeployment + sqlDeviceCheckDevice)

        //connect to the database
        this.connect()

        //Make the query and create a promise that handles the outcomes
        // let sqlPromise = this.query(sqlDeviceCheckDeployment + sqlDeviceCheckDevice)
        let sqlPromise = this.query(sqlDeviceCheckDevice)
        .then(result => {
            // console.log(result)
            // if length of the SELECT results are nonzero, then the device already exists and the insertion should not be done.
            // if (result[0].length > 0 || result[1].length > 0) 
            if (result.length > 0)
            {
                // console.log(result[0].length)
                // console.log(result[1].length)
                this.disconnect()
                return false
            }
            // Make the insertion queries
            // return this.query(sqlDeviceInsertDeployment + sqlDeviceInsertDevice)
            return this.query(sqlDeviceInsertDevice)
        }).catch(error => {
            console.error(error)
        })
        .then(result => { //thenify the promises for inserting the device. Note that inserting to the 'device' table will fail if it was already there, since the devEUI is the index key (i.e., it must be unique)
            
            this.disconnect()
            // basic pass to make sure it finished; should have non-zero insert-IDs
            if (result && result[0].insertId >= 0 && result[1].insertId >= 0) 
            if (result && result.insertId >= 0) 
            {
                return true
            }
            else return false
        }).catch(error => {
            this.disconnect()
            console.error(error)
            return false
        })
        return sqlPromise

    }

    /**
     * Retrieve the most recent sequence numbers (a versioning system) for a device and a particular schedule ID. This is not actually based on the version itself, but the time at which the schedules carrying that sequence number were added
     * 
     * @param devEUI The 16-character hex string uniquely identifying the EnviSense device
     * @param scheduleID The schedule identifer, a single character string of A, B, C, or D. Defaults to 'A'
     * @returns A promise for an array of (24) sequence numbers, each corresponding to an hour of the day
     */
    async getMostRecentSequenceNumbers(devEUI, scheduleID)
    {
        if (scheduleID === undefined) scheduleID = 'A'
        let devEUIupper = devEUI.toUpperCase()

        let tempTableName = "max_times_" + devEUIupper;
        let dropTempTable = "DROP TEMPORARY TABLE " + tempTableName + ";";

        //we'll create a temporary table to help find the max times. 
        let sqlTempTableMaxTimes =  "CREATE TEMPORARY TABLE " + tempTableName + " " +
            "SELECT " +
            "hour,  " +
            "MAX(time_created) as max_time " +
            "FROM schedule " +
            "WHERE DevEUI = \'" + devEUIupper + "\' AND scheduleID = \'" + scheduleID + "\' GROUP BY hour; "

        // Use a SQL query to find the most recent of these based on when the schedules were created
        let sqlLatestSequenceNumbers =  "SELECT schedule.hour, schedule.sequence " +
            "FROM schedule " +
            "INNER JOIN " + tempTableName + " " +
            "WHERE schedule.time_created = " + tempTableName + ".max_time AND " +
            "schedule.scheduleID = \'" + scheduleID + "\';";

        // We may have used this within another function that does this for many devices, so don't create or teardown the connection if it was already established
        let wasConnected = true
        if (this.connection == null) 
        {
            wasConnected = false
            this.connect()
        }

        //create the temporary table, find the most recent sequence numbers, and remove the temporary table
        var sqlPromise = this.query( sqlTempTableMaxTimes +  sqlLatestSequenceNumbers + dropTempTable)
        .then(result => {
            // console.log(result[0])
            let latestSequenceNumbers = result[1]; 
            let lsnLength = latestSequenceNumbers.length;

            // console.log("  DB responded with " + lsnLength + " results");
            // console.log(latestSequenceNumbers)
            // The DB responds with many duplicates of the same information... this is because some fields within the INNER JOIN are not unique. 
            //   Let's create a new object without duplicates (hacky)
            let lsn = Array(k.NUM_SCHEDULE_HOURS).fill(0);
            // let lsn = {}
            for (var i=0; i<lsnLength; i++)
            {
                var pair = latestSequenceNumbers[i];
                var hour = pair['hour'];
                var sequence = pair['sequence'];
                lsn[hour] = sequence;
            }
            if (!wasConnected) this.disconnect()

            return lsn
        }).catch(error => {
            if (!wasConnected) this.disconnect()
            console.error(error)
            return error
        })

        return sqlPromise
    }

    /**
     * Reterive a device schedule for a given schedule ID. Sequence numbers (versions) may be provided, but we'll default to using the most recent ones if those aren't provided. 
     * @param devEUI The 16-character hex string uniquely identifying the EnviSense device
     * @param scheduleID The schedule identifer, a single character string of A, B, C, or D. Defaults to 'A'
     * @param seqNumbers An array of sequence numbers (24 1-byte unsigned ints), defaulting to the most recent ones. 
     * @returns a promise for an ES_schedule, which contains 24 schedule blocks (1 for each hour)
     */
    async getDeviceSchedule(devEUI, scheduleID, seqNumbers)
    {
        devEUI = devEUI.toUpperCase()

        if (scheduleID === undefined) scheduleID = 'A'

        if (seqNumbers === undefined || seqNumbers.length !== k.NUM_SCHEDULE_HOURS) 
        {
            seqNumbers = await this.getMostRecentSequenceNumbers(devEUI, scheduleID)
        }

        //we'll create a bunch of promises and query for each hour individually
        let promises = []
        for (let i = 0; i < k.NUM_SCHEDULE_HOURS; i++) {
            promises.push(this.getDeviceScheduleForHour(devEUI, i, scheduleID, seqNumbers[i]))
        }

        // once everything comes back, return it as a new schedule
        let resultsPromise = Promise.all(promises)
        .then(schedules => {
            this.disconnect()
            return new schedule.ES_schedule(schedules, devEUI)

        }).catch(error => {
            console.error(error)
            this.disconnect()
            return error
        })

        return resultsPromise

    }

    /**
     *  Return the most recent schedule for the given hour
     * @param devEUI The 16-character hex string uniquely identifying the EnviSense device
     * @param hour The hour (0-23 int, inclusive) to query for. Defaults to 0
     * @param scheduleID The schedule identifer, a single character string of A, B, C, or D. Defaults to 'A'
     * @param seqNo The sequence number to query for. We'll retrieve the most recent of these, in the case that there are multiple
     * @returns A promise for an ES_schedule_block, which contains the set of instructions within this schedule block
     */
    async getDeviceScheduleForHour(devEUI, hour, scheduleID, seqNo)
    {
        if (scheduleID === undefined) scheduleID = 'A'
        if (hour === undefined) hour = 0
        if (devEUI === undefined) new Error('devEUI must have a value')
        devEUI = devEUI.toUpperCase()

        if (seqNo === undefined)
        {
            let newestSeqNo = await this.getMostRecentSequenceNumbers(devEUI, scheduleID) 
            seqNo = newestSeqNo[hour];

        }
        try 
        {
            let sqlScheduleQuery = "SELECT * " + 
                "FROM `schedule` " + 
                "WHERE devEUI='" + devEUI + "' AND scheduleID='" + scheduleID + "'" + 
                " AND hour=" + hour + " AND sequence=" + seqNo + " ORDER BY time_created DESC;" ;
                //ORDER BY time_created DESC will return results such that the newest is at index 0
            // console.log(sqlScheduleQuery)

            let wasConnected = true //only create and teardown the connetion if it didn't exist already. We use this function within other API calls frequently.
            if (this.connection == null) 
            {
                wasConnected = false
                this.connect()
            }

            //we'll return this promise for the schedule to better support asynchrony
            let sqlPromise = this.query(sqlScheduleQuery)
            .then(result => {
                if (!wasConnected) this.disconnect()
                // console.log(result)
                // if (result.length > 1) console.warn("Multiple schedule queries returned; may be duplicates of same sequence number. Returning first (most recent)")
                if (result.length === 0) {
                    console.error('No queries match devEUI %s for hour %d with scheduleID %s ', devEUI, hour, scheduleID)
                    return {}
                }
                let sched_response = Object.assign({}, result[0])
                let sched = schedule.ES_schedule_block.fromSqlResponse(sched_response)
                return sched
            }).catch(error => {
                if (!wasConnected) this.disconnect()
                console.error(error)
            })

            return sqlPromise 
        }
        catch (e)
        {
            console.error(e)
            throw e
        }

    }

    /**
     * Update the pending schedule for a device, as it is stored within the 'device' table
     * @param devEUI The 16-character hex string uniquely identifying the EnviSense device
     * @param scheduleID The schedule identifer, a single character string of A, B, C, or D. Defaults to 'A'
     * @returns A promise that resolves to true or false, depending on successful update or not
     */
    async updatePendingScheduleID(devEUI, scheduleID) 
    {
        let validIDs = ['A', 'B', 'C', 'D'] //TODO: either check what's available in the database or use some constant. Better to query. Make an asynchronous helper function
        if (validIDs.indexOf(scheduleID) == -1)
        {
            throw Error('Invalid schedule ID ' + scheduleID + '; should be one of ' + validIDs)
        }
        let devEUIupper = devEUI.toUpperCase()

        let wasConnected = true
        if (this.connection == null) 
        {
            wasConnected = false
            this.connect()
        }

        let sqlPendingScheduleUpdate = "UPDATE `device` " + 
            "SET `pendingScheduleID`='" + scheduleID  + "'" + 
            " WHERE `devEUI` = '" + devEUIupper + "' ;"
        console.log(sqlPendingScheduleUpdate)



        let sqlPromise = this.query(sqlPendingScheduleUpdate)
        .then(result => {
            console.log(result)
            if (!wasConnected) this.disconnect()

            if (result.affectedRows == 1) return true
            else return false
        }).catch(error => {
            console.error(error)
            if (!wasConnected) this.disconnect()
            return false
        })

        return sqlPromise
    }

    /**
     * Write a new schedule for a device
     * @param devEUI The 16-character hex string uniquely identifying the EnviSense device
     * @param newSchedule A ES_schedule, which contains 24 blocks of scheduler instructions
     * @returns A promise that resolves to a boolean to indicate if ALL schedules were written correctly o rnot
     */
    async writeNewSchedule(devEUI, newSchedule) 
    {
        if (!(newSchedule instanceof schedule.ES_schedule))
        {
            throw new Error('input schedule should be of type ES_schedule')
        }

        this.connect()
        //we'll just write these individually and separately to keep the insert simple, then we'll recombine all the results into a single true/false otput
        let promises = []
        newSchedule.blocks.forEach(scheduleBlock =>
        {
            promises.push(this.writeScheduleBlock(devEUI, scheduleBlock))
        })
        let resultsPromise = Promise.all(promises)
        .then(values => {
            this.disconnect()
            return values.every(Boolean)
        })
        .catch(error => { 
            this.disconnect()
            console.error(error); 
            return error
        })

        return resultsPromise

    }

    /**
     * Write a new schedule block for a device
     * @param devEUI The 16-character hex string uniquely identifying the EnviSense device
     * @param newSchedule A ES_schedule_block, which contains the set of scheduler instructions, the hour to write, the sequence number to write, and the schedule ID
     * @returns A promise that resolves to a boolean to indicate if ALL schedules were written correctly or not
     */
    async writeScheduleBlock(devEUI, scheduleBlock)
    {
        let wasConnected = true
        if (this.connection == null) 
        {
            wasConnected = false
            this.connect()
        }
        if (!(scheduleBlock instanceof schedule.ES_schedule_block))
        {
            throw new Error('input schedule block should be of type ES_schedule_block')
        }

        //FIXME: check to ensure this device already exists in the 'device' table

        let sqlScheduleInsertQuery =  "INSERT INTO `schedule` " + 
            "(`id`, `time_created`, `DevEUI`, `scheduleID`, `hour`, `sequence`, " + 
            "`seconds_0`, `action_0`, `parameter_0`, " + 
            "`seconds_1`, `action_1`, `parameter_1`, " + 
            "`seconds_2`, `action_2`, `parameter_2`, " + 
            "`seconds_3`, `action_3`, `parameter_3`, " + 
            "`seconds_4`, `action_4`, `parameter_4`, " + 
            "`seconds_5`, `action_5`, `parameter_5`, " + 
            "`seconds_6`, `action_6`, `parameter_6`, " + 
            "`seconds_7`, `action_7`, `parameter_7`, " + 
            "`seconds_8`, `action_8`, `parameter_8`, " + 
            "`seconds_9`, `action_9`, `parameter_9`, " + 
            "`seconds_10`, `action_10`, `parameter_10`) " + 

            " VALUES (NULL, CURRENT_TIMESTAMP, '"  + 
            devEUI.toUpperCase() + "', '" + 
            scheduleBlock.scheduleID + "', '" + scheduleBlock.hour + "', '" + scheduleBlock.sequenceNo + "', " 

        for (let i = 0; i < 11; i++)
        {
            let instr = scheduleBlock.instructions[i]
            
            if (instr === undefined) 
            {
                sqlScheduleInsertQuery += "'3599', 'NO_OPERATION', '0'"
            }
            else
            {
                sqlScheduleInsertQuery += instr.toQueryString()
            }

            if (i < 10) {
                sqlScheduleInsertQuery += ','
            }
        }
        sqlScheduleInsertQuery += ') ;'

        // console.log(sqlScheduleInsertQuery)
        let sqlPromise = this.query(sqlScheduleInsertQuery)
        .then(result => {
            if (!wasConnected) this.disconnect()

            if (result.affectedRows == 1 && result.insertId > 0) return true
            else return false
        }).catch(error => {
            console.error(error)
            if (!wasConnected) this.disconnect()
            return false

        })

        return sqlPromise

    }

    
}


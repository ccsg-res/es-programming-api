/**
 * 
 * A file to run tests on the API's mechanisms. Intended more for debbugging than continuous integration
 * 
 * Created by Reese Grimsley, July 2021
 */

import * as cs from './es_chirpstack.mjs'
import * as sql from './es_sql.mjs'
import * as k from './es_constants.mjs'
import * as schedule from './es_schedule.mjs'
// require('./es_chirpstack.mjs')


import { createRequire } from "module"; // in classic JS form, using 'require' and 'import in the same file/module is a no-no, and in classic-er form, there is (probably unsafe) workaround. Asinine.
const require = createRequire(import.meta.url);
const config = require("./config.json");


// let devices = await getDevices() //test an export
// console.log(devices)
async function insertExampleDeviceIntoChirpstack() {
    let appkey = 'ffaaf4e16a1328417d89b8466ebf1401';
    let deveui = '0000000000000000';
    let devicename = 'test-device'
    let deviceProfileId = 'f223d4d7-922d-437a-bdd6-dd8a20924f21' // this UUID is hard to find... it's actually the last string in the URL when looking at the device profile. This field is not actualy necessary
    let applicationID = 1 // under applicatoins, there is a column with IDs. Use that.
    // let success = await addNewDevice(deveui, devicename, appkey, deviceProfileId, applicationID)
    let success = await cs.addNewDevice(deveui, devicename)

    console.log(success)
    if (success)
        console.log('We did it!')
}

export async function createNewDevice(devEUI, deviceName, useSDI12)
{
    if (useSDI12 == undefined) useSDI12 = false

    let sqlClient = new sql.ES_sql(config.mysql)

    let chirpstackAddDevicePromise = cs.addNewDevice(devEUI, deviceName)
    let chirpstack_result = await chirpstackAddDevicePromise
    if (chirpstack_result === false) return chirpstack_result
    console.log('Added device to chirpstack')


    let sqlAddDevicePromise = sqlClient.insertNewDevice(devEUI, deviceName)
    let sql_add_device_result = await sqlAddDevicePromise
    if (sql_add_device_result === false) return sql_add_device_result
    console.log('Added device to SQL database')

    if (useSDI12)
    {
        var scheduleA = schedule.generateDefaultSDI12Schedule()
        var scheduleB = schedule.generateHourlySDI12SamplingSchedule()
        var scheduleC = schedule.generate6HourlySDI12SamplingSchedule()
        //we'll leave schedule D to be the regular default schedule
        var scheduleD = schedule.generateDefaultSchedule()
    }
    else 
    {
        var scheduleA = schedule.generateDefaultSchedule()
        var scheduleB = schedule.generateHourlySamplingSchedule()
        var scheduleC = schedule.generate6HourlySamplingSchedule()
        var scheduleD = schedule.generateDefaultSchedule()
    }

    scheduleA.setScheduleIDs('A')
    scheduleA.setSequenceNumbers(1)
    let writeScheduleAPromise = sqlClient.writeNewSchedule(devEUI, scheduleA)
    let write_schedule_a_result = await writeScheduleAPromise
    if (write_schedule_a_result === false) return write_schedule_a_result
    console.log('Added default schedule A for device to SQL database')

    scheduleB.setScheduleIDs('B')
    scheduleB.setSequenceNumbers(1)
    let writeScheduleBPromise = sqlClient.writeNewSchedule(devEUI, scheduleB)
    let write_schedule_b_result = await writeScheduleBPromise
    if (write_schedule_b_result === false) return write_schedule_b_result
    console.log('Added default schedule B for device to SQL database')


    scheduleC.setScheduleIDs('C')
    scheduleC.setSequenceNumbers(1)
    let writeScheduleCPromise = sqlClient.writeNewSchedule(devEUI, scheduleC)
    let write_schedule_c_result = await writeScheduleCPromise
    if (write_schedule_c_result === false) return write_schedule_c_result
    console.log('Added default schedule C for device to SQL database')


    scheduleD.setScheduleIDs('D')
    scheduleD.setSequenceNumbers(1)
    let writeScheduleDPromise = sqlClient.writeNewSchedule(devEUI, scheduleD)
    let write_schedule_d_result = await writeScheduleDPromise
    if (write_schedule_d_result === false) return write_schedule_d_result
    console.log('Added default schedule D for device to SQL database')

    console.log("Successfully added new device (" + devEUI + ", " + deviceName + ") to chirpstack and SQL database")

    return true
}

export async function readDeviceSchedule(devEUI, scheduleID)
{
    let sql_client = new sql.ES_sql(config.mysql)

    return await sql_client.getDeviceSchedule(devEUI, scheduleID)

}

export async function writeDeviceSchedule(devEUI, schedule)
{
    let sql_client = new sql.ES_sql(config.mysql)


    return await sqlClient.writeNewSchedule(devEUI, schedule)

}



/***TESTING***/

let sqlClient = new sql.ES_sql(config.mysql)

// let instructions = await sqlClient.getSchedulerInstructions()
// console.log(instructions)
// let result = await sqlClient.insertNewDevice('0000000700000000', 'test-device', undefined, 0)
// console.log(result)

// let str = sql.defaultScheduleEntryInsertString(test_dev_eui, 'A', 3, 1)
// console.log(str)

// result = await sqlClient.getMostRecentSequenceNumbers('00000007C103C1EB', 'A')
// console.log('result')
// console.log(result)
// console.log(k.NUM_SCHEDULE_HOURS)

// result = await sqlClient.getDeviceScheduleForHour('00000007C103C1EB', 0, 'A')
// console.log(result)
// let s = schedule.ES_schedule_block.fromSqlResult(result)
// console.log(s)


// let ss = schedule.defaultSchedule
// ss.setScheduleIDs('B')
// ss.setSequenceNumbers(11)
// console.log('default schedule')
// console.log(ss.toString())
// console.log('\n\n')

// ss = schedule.defaultSDI12Schedule
// console.log('default SDI12 schedule')
// console.log(ss.toString())
// console.log('\n\n')


// ss = schedule.hourlySchedule
// console.log('default houlry schedule')
// console.log(ss.toString())
// console.log('\n\n')

// ss = schedule.hourlySDI12Schedule
// console.log('default SDI12 houlry schedule')
// console.log(ss.toString())
// console.log('\n\n')

// ss = schedule.sixHourlySchedule
// console.log('default 6 houlry schedule')
// console.log(ss.toString())
// console.log('\n\n')

// ss = schedule.sixHourlySDI12Schedule
// console.log('default 6 houlry SDI schedule')
// console.log(ss.toString())
// console.log('\n\n')


// result = await sqlClient.updatePendingScheduleID(test_dev_eui, 'B')
// console.log(result) 

// console.log('write schedules')
// await sqlClient.writeNewSchedule(test_dev_eui, ss)

// createNewDevice(test_dev_eui, test_dev_name)


// let scheduleA = schedule.generateDefaultSchedule()
// scheduleA.setScheduleIDs('A')
// scheduleA.setSequenceNumbers(3)
// let x = await sqlClient.writeNewSchedule(test_dev_eui, scheduleA)
// console.log(x)


// let sched = await sqlClient.getDeviceScheduleForHour(test_dev_eui, 0, 'A')
// console.log(sched.toString())

// let all_sched = await sqlClient.getDeviceSchedule(test_dev_eui, 'A')
// console.log(all_sched.toString())
// console.log(schedule.generateDefaultSchedule().toString())
/**
 * 
 * An API for access Chirpstack, primarily for listing the set of existing devices and adding new devices
 * 
 * Created by Reese Grimsley, July 2021
 */

import * as grpc from '@grpc/grpc-js';

import * as internalService from '@chirpstack/chirpstack-api/as/external/api/internal_grpc_pb.js';
import * as internalMessages from '@chirpstack/chirpstack-api/as/external/api/internal_pb.js';
import * as device from '@chirpstack/chirpstack-api/as/external/api/device_pb.js'
import * as deviceServices from '@chirpstack/chirpstack-api/as/external/api/device_grpc_pb.js'

import { createRequire } from "module"; // in classic JS form, using 'require' and 'import in the same file/module is a no-no, and in classic-er form, there is a (probably unsafe) workaround. Asinine.
import { create } from 'domain';
const require = createRequire(import.meta.url);

import * as k from './es_constants.mjs'

const config = require("./config.json");

/**
 * 
 * @param chirpstackConfig configuration, which should contain a host (IP address or url), port, username, and password. It should also contain a default application/network key, a default device profile ID (UUID obtained from chirpstack in the URL), and a default application ID
 * @returns A promise for login credentials (GRPC metadata)
 */
export async function setupChirpstackConnection(chirpstackConfig)
{
    if (chirpstackConfig === undefined) {chirpstackConfig = config.chirpstack}

    let host = chirpstackConfig.host + ':' + chirpstackConfig.port
    // console.log("[chirpstack ] Setup connection for chirpstack host at " + host)
// Create the client for the 'internal' service
    let internalServiceClient = new internalService.InternalServiceClient(
        host,
        grpc.credentials.createInsecure() //we'll populate this by making a request, which should obtain a JWT (authentication token)
    )

    const loginRequest = new internalMessages.default.LoginRequest();   //must insert 'default' to recognize function

    loginRequest.setEmail(chirpstackConfig.user);
    loginRequest.setPassword(chirpstackConfig.password);
    
    let loginPromise = new Promise((resolve, reject) => internalServiceClient.login(loginRequest, (error, response) => {
        // Build a gRPC metadata object, setting the authorization key to the JWT we
        if (error) {
            return reject(error)
        }
        resolve(response)
    })).then(result => {
        // got back from logging in. Add results to the metadata, which we'll need to include in future requests as part of the authentication 
        var metadata = new grpc.Metadata();
        metadata.set('authorization', result.getJwt());
        metadata.set('address', config.chirpstack.host + ":" + config.chirpstack.port)
        return metadata
    }).catch(error=> {
        console.log('Failed login to chirpstack')
        console.error(error)
    })

    return loginPromise
}

/**
 * Get the set of devices listed in Chirpstack, independent of the application. 
 * @param chirpstackConfig configuration, generally obtained from a file. Requires a host, port, username and password
 * @param authMetadata Optional metadata. If this has already been obtained by setting upa  chirpstack connection, provide that here. Otherwise, we'll create that.
 * @returns A promise that resolves to an array of objects containing device EUI's and device names. 
 */
export async function getDevices(chirpstackConfig, authMetadata)
{
    if (chirpstackConfig === undefined) {chirpstackConfig = config.chirpstack}
    if (authMetadata === undefined) {authMetadata = await setupChirpstackConnection(chirpstackConfig)}

    let deviceServiceClient = new deviceServices.DeviceServiceClient(
        chirpstackConfig.host + ":" + chirpstackConfig.port,
        grpc.credentials.createInsecure()
    );

    let listDeviceRequest = new device.default.ListDeviceRequest()
    // //can set a few filters, if desired
    // listDeviceRequest.setSearch("00000007") 
    // listDeviceRequest.setApplicationId("CCSG-Test")
    // listDeviceRequest.setApplicationId(1)
    listDeviceRequest.setLimit(100000) // must set some limit (even if stupidly big), or the set of results will be empty

    let devicesPromise = new Promise((resolve, reject) => deviceServiceClient.list(listDeviceRequest, authMetadata, (error, response) => {
        // Build a gRPC metadata object, setting the authorization key to the JWT we
        if (error) {
            return reject(error)
        }
        resolve(response)
    })).then(result => {
        //reformat the results into a list containing the devEUI and deviceName only, as that's all we really need in the rest of the API
        let devices = []
        result.getResultList().forEach(device => {
            devices.push(
                {
                    'devEUI': device.getDevEui(),
                    'deviceName': device.getName()
                }
            )
        });
        return devices

    }).catch(error=> {
        console.error(error)
    })

    return devicesPromise
}

/**
 * Create a new device for chirpstack. Only the newDeviceDevEUI and newDeviceName absolutely require specification, as the rest can be obtained from the config.json file. Note that this will run into issues if the LoRaWAN version is 1.1.0 or newer, as they slightly changed how appkeys and network keys work.
 * @param newDeviceDevEui The 16-character hex string that uniquely identifies this device
 * @param newDeviceName A descriptive naem (string) for the device
 * @param applicationKey An application key that the device will use to identify itself as part of over-the-air authetnication. Default pulled from config.json
 * @param deviceProfileID A profile ID for the device, which can be found in the URL when looking at a device profile in the chirpstack web UI. Default pulled from config.json
 * @param applicationID A application IDto associate with this device. This is related to the MQTT bus that we listen to when servicing devices in the field as they communicate with the network via LoRaWAN. Default pulled from config.json
 * @param description An optional description for the device, purely for informative purposes
 * @param chirpstackConfig A chirpstack configuration object, which must have (at a minimum) the chirpstack host, port, a username and a password. See config.json for an example
 * @param authMetadata Authentication metadata that contains a JWT token 
 * @returns A promise that indicates true or false depending on the success of adding the device.
 */
export async function addNewDevice(newDeviceDevEui, newDeviceName, applicationKey, deviceProfileID, applicationID, description, chirpstackConfig, authMetadata)
{
    if (chirpstackConfig === undefined) {chirpstackConfig = config.chirpstack}
    if (authMetadata === undefined) {authMetadata = await setupChirpstackConnection(chirpstackConfig)}

    let deviceServiceClient = new deviceServices.DeviceServiceClient(
        chirpstackConfig.host + ":" + chirpstackConfig.port,
        grpc.credentials.createInsecure()
    );

    //setup a new device
    let newDevice = new device.default.Device()
    newDevice.setDevEui(newDeviceDevEui)
    newDevice.setName(newDeviceName)
    if (deviceProfileID === undefined) deviceProfileID = chirpstackConfig.default_deviceProfileID
    newDevice.setDeviceProfileId(deviceProfileID) //#TODO; figure out which ones exist (or have a default name..) so we can assign something in case the field is empty
    if (applicationID === undefined) {applicationID = chirpstackConfig.default_applicationID}
    newDevice.setApplicationId(applicationID)
    if (description === undefined) {description = "EnviSense Environmental Data Logger - " + newDeviceName}
    newDevice.setDescription(description)

    let createDeviceRequest = new device.default.CreateDeviceRequest()
    createDeviceRequest.setDevice(newDevice)

    // configure a promise that will send the device request to the chirpstack host.
    let createDevicePromise = new Promise((resolve, reject) => deviceServiceClient.create(createDeviceRequest, authMetadata, (error, response) => {
        // Build a gRPC metadata object, setting the authorization key to the JWT we
        if (error) {
            console.error(error)
            return reject(error)
        }
        resolve(response)
    })).then(result => {
        console.log('result of device add')
        console.log(result)
        
        //setup keys for the device
        let deviceKeys = new device.default.DeviceKeys()
        deviceKeys.setDevEui(newDeviceDevEui)
        if (applicationKey === undefined) applicationKey = chirpstackConfig.default_appkey 
        deviceKeys.setNwkKey(applicationKey) //in LoRaWAN <1.1.0, the appkey as we know it is understood as the network key. In newer versions, there are both. In that case, FIXME...
    
        let createKeysRequest = new device.default.CreateDeviceKeysRequest()
        createKeysRequest.setDeviceKeys(deviceKeys)

        return new Promise((resolve, reject) => deviceServiceClient.createKeys(createKeysRequest, authMetadata, (error, response) => {
            // Build a gRPC metadata object, setting the authorization key to the JWT we
            if (error) {
                return reject(error)
            }
            resolve(response)
   
    }))}).catch(error => {
        console.log('error of device-add')
        console.error(error)
        return false
    }) //respond to the promise; it will return false if the request to createKeys failed.
    .then(result => {
        console.log(result)
        if (result === false) return false
        return true
    }).catch(error => {
        console.log(error)
        return false
    })

    return createDevicePromise
}
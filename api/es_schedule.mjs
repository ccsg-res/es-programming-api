/**
 * 
 * Defintions for schedules objects to help with abstractions within the API
 * 
 * Created by Reese Grimsley, July 2021
 */

import * as k from './es_constants.mjs'

export class ES_schedule 
{
    constructor(blocks, devEUI) {
        let allValid = blocks.every(b =>
            {
                return (b instanceof ES_schedule_block)
            })
        if (allValid && blocks.length == k.NUM_SCHEDULE_HOURS)
        {
            this.blocks = blocks //this should be an array (of 24)
        } 
        else throw Error('blocks are of invalid format')

        if (devEUI !== undefined) this.devEUI = devEUI
    }
    
    setScheduleIDs(scheduleID) {
        if (typeof scheduleID === 'string')
        {
            this.blocks.forEach(block => {
                
                block.setScheduleID(scheduleID)
            })
        }
        else throw new Error('Schedule ID must be a string')
    }

    setSequenceNumbers(sequenceNo) 
    {
        if (typeof sequenceNo == 'number')
        {

            this.blocks.forEach(block => {
            
                block.setSequenceNumber(sequenceNo)
            })
        } 
        else if (Array.isArray(sequenceNo))
        {
            for (let i=0; i<sequenceNo.length; i++)
            {
                this.blocks[i].setSequenceNumber(sequenceNo[i])
            }
        }
        else throw new Error('Improper set of sequence numbers; should be one or an array (one for each schedule block')
    }

    isIdenticalBlocks()
    {
        let first_block = this.blocks[0]
        for (let i = 1 ; i < this.blocks.length; i++)
        {
            if (! first_block.isIdenticalBlockInstructions(this.blocks[i]))
            {
                console.log('blocks not identical; block ' + i + ' is different')

                return false
            }
        }
        return true
    }

    setDevEUI(devEUI)
    {
        this.devEUI = devEUI
    }

    getDevEUI()
    {
        if (this.devEUI !== undefined) return this.devEUI
        else return undefined
    }

    toString()
    {
        let output_str = 'ES Schedule blocks=['
        for (let i = 0 ; i < this.blocks.length; i++)
        {
            output_str += '\n\t'
            output_str += this.blocks[i].toString()
            if (i < this.blocks.length-1)
                output_str += '\t,\n'
        }


        output_str += '    ]\n'
        return output_str
    }
}

export class ES_schedule_block
{
    constructor(instructions, hour, sequenceNo=0, scheduleID='A', devEUI)
    {
        if (instructions.length > 11 || instructions.length === undefined) 
        {
            throw Error("Only 11 instructions can be used for a single schedule block")
        }

        if (sequenceNo < 0 || sequenceNo > 255) 
        {
            console.error('Sequence number should be in [0, 255]')
            sequenceNo = sequenceNo % 256
            if (sequenceNo < 0)
            {
                    sequenceNo += 256
            }
        }

        if (hour < 0 || hour > k.NUM_SCHEDULE_HOURS)
        {
            console.error('Hour should be in [0, ' + k.NUM_SCHEDULE_HOURS + ']')
            //could also modulo this...
            hour = hour % k.NUM_SCHEDULE_HOURS
            if (sequenceNo < 0)
            {
                    sequenceNo += k.NUM_SCHEDULE_HOURS
            }
        }

        let validIDs = ['A', 'B', 'C', 'D'] //TODO: either check what's available in the database or use some constant. Better to query. Make an asynchronous helper function
        if (validIDs.indexOf(scheduleID) == -1)
        {
            console.error('Invalid schedule ID ' + scheduleID + '; should be one of ' + validIDs + '; setting to default of "A"')
            scheduleID = 'A'
        }

        if (devEUI !== undefined) this.devEUI = devEUI

        this.instructions = instructions
        this.hour = hour
        this.sequenceNo = sequenceNo
        this.scheduleID = scheduleID

    }

    setHour(hour) 
    {
        if (hour < 0 || hour > k.NUM_SCHEDULE_HOURS)
        {
            throw Error('Hour should be in [0, ' + k.NUM_SCHEDULE_HOURS + ']')
        }

        this.hour = hour
    }

    setScheduleID(scheduleID) {

        let validIDs = ['A', 'B', 'C', 'D'] //TODO: either check what's available in the database or use some constant. Better to query. Make an asynchronous helper function
        if (validIDs.indexOf(scheduleID) == -1)
        {
            throw Error('Invalid schedule ID ' + scheduleID + '; should be one of ' + validIDs + '; setting to default of "A"')
        }
        this.scheduleID = scheduleID
    }

    setSequenceNumber(seqNo) {

        if (seqNo < 0 || seqNo > 255) 
        {
            throw new Error('Sequence number should be in [0, 255]')
        }
        this.sequenceNo = seqNo
    }

    static fromSqlResult(sqlResult) 
    {
        let instructions = []
        for (let i = 0; i < 11; i++)
        {
            let instruction = new ES_schedule_instruction(sqlResult['seconds_'+i], sqlResult['action_'+i], sqlResult['parameter_'+i])

            instructions.push(instruction)
        }   
        let hour = sqlResult.hour
        let sequenceNo = sqlResult.sequence
        let scheduleID = sqlResult.scheduleID

        return new ES_schedule_block(instructions, hour, sequenceNo, scheduleID)
    }

    isIdenticalBlockInstructions(block)
    {

        if (! block instanceof ES_schedule_block) return false
        if (this.instructions.length != block.instructions.length) return false
        
        for (let i = 0; i < this.instructions.length; i++) 
        {
            let this_instr = this.instructions[i]
            let that_instr = block.instructions[i]

            if (this_instr.seconds != that_instr.seconds ||
                this_instr.parameter != that_instr.parameter ||
                this_instr.instruction != that_instr.instruction)
            {
                return false
            }
        }
        return true
    }

    toString()
    {
        let output_str = ''
        output_str += 'ES Schedule block ID=' + this.scheduleID + ', hour=' + this.hour + ', sequenceNo=' + this.sequenceNo
        output_str += ', Instructions: ['
        for (let i = 0; i < this.instructions.length; i++) 
        {
            output_str += '\n\t'
            output_str += this.instructions[i].toString()
            if (i < this.instructions.length-1)
                output_str += ','
        }
        output_str += '    ]'

        return output_str
    }

    static fromSqlResponse(response)
    {
        let hour =  response.hour
        let scheduleID = response.scheduleID
        let sequence = response.sequence

        let instructions = []
        for (let i = 0; i < 11; i++) {
            let instr = new ES_schedule_instruction(response['seconds_'+i], response['action_'+i], response['parameter_'+i])
            instructions.push(instr)
        }

        return new ES_schedule_block(instructions, hour, sequence, scheduleID)

    }

    setDevEUI(devEUI)
    {
        this.devEUI = devEUI
    }

    getDevEUI()
    {
        if (this.devEUI !== undefined) return this.devEUI
        else return undefined
    }

}

export class ES_schedule_instruction 
{
    constructor(seconds, instruction, parameter)
    {
        if (seconds >= k.MAX_SCHEDULE_TIME  || seconds < 0) throw Error('Time offset for the instruction must be positive and < ' + k.MAX_SCHEDULE_TIME)
        this.seconds = seconds

        if (parameter > 255 || parameter < 0) throw Error('Instruction parameter must be in [0, 255]')
        this.parameter = parameter

        this.instruction = instruction //cannot check without querying database

    }
    
    toString()
    {
        return 'T=' + this.seconds + '\t"' + this.instruction + '"\t(' + this.parameter + ')'
    }

    toQueryString()
    {
        return " '" + this.seconds + "', '" + this.instruction + "', '" + this.parameter + "'"
    }
}

export function generateDefaultSchedule() 
{
    let instr0 = new ES_schedule_instruction(0, 'READ_KITCHEN_SINK', 0)
    let instr1 = new ES_schedule_instruction(0, 'SEND_LORA_FRAME_NOW', 10)
    let instr2 = new ES_schedule_instruction(900, 'READ_KITCHEN_SINK', 0)
    let instr3 = new ES_schedule_instruction(900, 'SEND_LORA_FRAME_NOW', 10)
    let instr4 = new ES_schedule_instruction(1800, 'READ_KITCHEN_SINK', 0)
    let instr5 = new ES_schedule_instruction(1800, 'SEND_LORA_FRAME_NOW', 10)
    let instr6 = new ES_schedule_instruction(2700, 'READ_KITCHEN_SINK', 0)
    let instr7 = new ES_schedule_instruction(2700, 'SEND_LORA_FRAME_NOW', 10)
    let instr8 = new ES_schedule_instruction(3000, 'DOWNLOAD_SCHEDULE', 0)
    let instr9 = new ES_schedule_instruction(3000, 'NO_OPERATION', 0)
    let instr10 = new ES_schedule_instruction(3000, 'NO_OPERATION', 0)

    let instructions = [instr0, instr1, instr2, instr3, instr4, instr5, instr6, instr7, instr8, instr9, instr10]

    let schedule_blocks = []
    for (let i = 0 ; i < k.NUM_SCHEDULE_HOURS; i++)
    {
        schedule_blocks.push(new ES_schedule_block(instructions, i))
    }
    
    return new ES_schedule(schedule_blocks)

}

export function generateDefaultSDI12Schedule() 
{
    //by default, use the SDI12 Kitchen sink parameter of 1 so that all other sensors are sampled too
    let instr0 = new ES_schedule_instruction(0, 'READ_SDI12_KITCHEN_SINK', 1)
    let instr1 = new ES_schedule_instruction(0, 'SEND_LORA_FRAME_NOW', 10)
    let instr2 = new ES_schedule_instruction(900, 'READ_SDI12_KITCHEN_SINK', 1)
    let instr3 = new ES_schedule_instruction(900, 'SEND_LORA_FRAME_NOW', 10)
    let instr4 = new ES_schedule_instruction(1800, 'READ_SDI12_KITCHEN_SINK', 1)
    let instr5 = new ES_schedule_instruction(1800, 'SEND_LORA_FRAME_NOW', 10)
    let instr6 = new ES_schedule_instruction(2700, 'READ_SDI12_KITCHEN_SINK', 1)
    let instr7 = new ES_schedule_instruction(2700, 'SEND_LORA_FRAME_NOW', 10)
    let instr8 = new ES_schedule_instruction(3000, 'DOWNLOAD_SCHEDULE', 0)
    let instr9 = new ES_schedule_instruction(3000, 'NO_OPERATION', 0)
    let instr10 = new ES_schedule_instruction(3000, 'NO_OPERATION', 0)

    let instructions = [instr0, instr1, instr2, instr3, instr4, instr5, instr6, instr7, instr8, instr9, instr10]

    let schedule_blocks = []
    for (let i = 0 ; i < k.NUM_SCHEDULE_HOURS; i++)
    {
        schedule_blocks.push(new ES_schedule_block(instructions, i))
    }
    
    return new ES_schedule(schedule_blocks)

}

export function generateHourlySamplingSchedule() 
{
    let instr0 = new ES_schedule_instruction(0, 'READ_KITCHEN_SINK', 0)
    let instr1 = new ES_schedule_instruction(0, 'SEND_LORA_FRAME_NOW', 10)
    let instr2 = new ES_schedule_instruction(0, 'DOWNLOAD_SCHEDULE', 0)
    let instr3 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr4 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr5 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr6 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr7 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr8 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr9 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr10 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)

    let instructions = [instr0, instr1, instr2, instr3, instr4, instr5, instr6, instr7, instr8, instr9, instr10]

    let schedule_blocks = []
    for (let i = 0 ; i < k.NUM_SCHEDULE_HOURS; i++)
    {
        schedule_blocks.push(new ES_schedule_block(instructions, i))
    }
    
    return new ES_schedule(schedule_blocks)

}

export function generateHourlySDI12SamplingSchedule() 
{
    //by default, use the SDI12 Kitchen sink parameter of 1 so that all other sensors are sampled too
    let instr0 = new ES_schedule_instruction(0, 'READ_SDI12_KITCHEN_SINK', 1)
    let instr1 = new ES_schedule_instruction(0, 'SEND_LORA_FRAME_NOW', 10)
    let instr2 = new ES_schedule_instruction(0, 'DOWNLOAD_SCHEDULE', 0)
    let instr3 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr4 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr5 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr6 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr7 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr8 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr9 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr10 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)

    let instructions = [instr0, instr1, instr2, instr3, instr4, instr5, instr6, instr7, instr8, instr9, instr10]

    let schedule_blocks = []
    for (let i = 0 ; i < k.NUM_SCHEDULE_HOURS; i++)
    {
        schedule_blocks.push(new ES_schedule_block(instructions, i))
    }
    
    return new ES_schedule(schedule_blocks)

}

export function generate6HourlySamplingSchedule() 
{
    let instr0 = new ES_schedule_instruction(0, 'READ_KITCHEN_SINK', 0)
    let instr1 = new ES_schedule_instruction(0, 'SEND_LORA_FRAME_NOW', 10)
    let instr2 = new ES_schedule_instruction(0, 'DOWNLOAD_SCHEDULE', 0)
    let instr3 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr4 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr5 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr6 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr7 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr8 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr9 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr10 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)

    let null_instr = new ES_schedule_instruction(0, 'NO_OPERATION', 0)

    let instructions = [instr0, instr1, instr2, instr3, instr4, instr5, instr6, instr7, instr8, instr9, instr10]

    let null_instrs = [null_instr, null_instr, null_instr, null_instr, null_instr, null_instr, null_instr, null_instr, null_instr, null_instr, null_instr]

    let schedule_blocks = []
    for (let i = 0 ; i < k.NUM_SCHEDULE_HOURS; i++)
    {
        if (i % 6 == 0)
        {
            schedule_blocks.push(new ES_schedule_block(instructions, i))
        }
        else{
            schedule_blocks.push(new ES_schedule_block(null_instrs, i))
        }
    }
    
    return new ES_schedule(schedule_blocks)

}

export function generate6HourlySDI12SamplingSchedule() 
{
    //by default, use the SDI12 Kitchen sink parameter of 1 so that all other sensors are sampled too
    let instr0 = new ES_schedule_instruction(0, 'READ_SDI12_KITCHEN_SINK', 1)
    let instr1 = new ES_schedule_instruction(0, 'SEND_LORA_FRAME_NOW', 10)
    let instr2 = new ES_schedule_instruction(0, 'DOWNLOAD_SCHEDULE', 0)
    let instr3 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr4 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr5 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr6 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr7 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr8 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr9 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)
    let instr10 = new ES_schedule_instruction(0, 'NO_OPERATION', 0)

    let null_instr = new ES_schedule_instruction(0, 'NO_OPERATION', 0)

    let instructions = [instr0, instr1, instr2, instr3, instr4, instr5, instr6, instr7, instr8, instr9, instr10]

    let null_instrs = [null_instr, null_instr, null_instr, null_instr, null_instr, null_instr, null_instr, null_instr, null_instr, null_instr, null_instr]

    let schedule_blocks = []
    for (let i = 0 ; i < k.NUM_SCHEDULE_HOURS; i++)
    {
        if (i % 6 == 0)
        {
            schedule_blocks.push(new ES_schedule_block(instructions, i))
        }
        else{
            schedule_blocks.push(new ES_schedule_block(null_instrs, i))
        }
    }
    
    return new ES_schedule(schedule_blocks)

}


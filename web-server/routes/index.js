/**
 * 
 * The expressJS routing file that responds to HTTP GETs and POSTs within the web server
 * 
 * Created by Reese Grimsley, July 2021
 */

import express from 'express'
var router = express.Router();

import * as es_api from '../../api/api.mjs'



/* 
GET home page. Query the API for a set of available devices and the set of instructions. We won't repopulate those until we reload the page 
*/
router.get('/', function(req, res, next) {
  // res.render('index', {title:'temp'})
  var devices, instructions
  es_api.getCSDevices().then(result =>
    {
      devices = result
      devices = devices.sort()
      // res.render('index', {devices: result, title:'EnviSense Programming API'})
      return es_api.getInstructions()
    }).catch(err => {
      console.error(err)
      // res.send("Failed to retrieve devices" + err)
    })
    .then(result => {
      instructions = result
      res.render('index', {devices: devices, title:'EnviSense Programming API', instructions:instructions})
    }).catch(err => {
      console.error(err)
      // res.send("Failed to retrieve devices" + err)
    })
});

router.post('/', function(req, res) {
  res.send('POST to main page')
})

/*
Read a schedule for a set of devices and hours. The GET command will have this information include in the request, which is encoded into the URL
*/
router.get('/devices/readSchedule',  function(req, res, next)
{
  try
  {
    // console.log(req)
    // 'req' includes the GET data within the URL, but it can be retrieved using the 'query' part of te request
    console.log('readSchedule GET request received')
    let hours = req.query.hours
    if (hours === undefined || hours.length === 0)
    {
      console.log('bad request ; no hours')
      res.status(400).send('No hours provided')
      return
    }

      
    let scheduleID = req.query.scheduleID
    if (scheduleID === undefined)
    {
      console.log('bad request ; no scheduleID')
      res.status(400).send('No scheduleID present')
      return
    }

    let devices = req.query.devices
    if (devices === undefined || devices.length === 0)
    {
      console.log('bad request ; no devices')
      res.status(400).send('No devices provided')
      return
    }

    //Each device query is going to return a promise (or set of promises), the results of which contain the query (or a failure indicator).
    let promises = []
    devices.forEach(dev => {
        promises.push(es_api.readDeviceSchedule(dev, scheduleID))
    })

    //create a promise composed of all the promises, and when that lazy evaluation completes, send all the schedules in an array back to the browser
    Promise.all(promises)
    .then(schedules => {
      console.log('all device schedue reads returned')
      let output = []
      schedules.forEach(schedule =>  {
          output.push(schedule)
        })
      // console.log(output)
      res.send(output)
    }).catch(error => {
      console.log(error)
      res.status(500).send(error)
    })
  } catch (error)
  {
    console.error(error)
  }
})

/*
Create a device by adding it to Chirpstack and the SQL database. When adding it to SQL, also create a series of default schedules so the device has something to download on first boot. The return of this will indicate which components of this process passed (true) or failed (false)
*/
router.post('/devices/createDevice', function(req, res, next) 
{

  try {
    let devEUI = req.body.devEUI.toUpperCase()
    let deviceName = req.body.deviceName
    let isSdi12 = req.body.isSdi12
  
    console.log('Create device named "' + deviceName + '" with devEUI "' + devEUI + '"')
    if (isSdi12) console.log('Default schedules will be for SDI12')

    let prom = es_api.createNewDevice(devEUI, deviceName, isSdi12)
    .then(result => {
      console.log('returned results: ' + JSON.stringify(result))
      res.send(result)
    }).catch(error => {
      console.log('error when adding device')
      console.error(error)
      console.log('that was the error')
      res.status(500).send(error)
    })
  } catch (error) {

    console.error(error)
    console.log('error in /devices/createDevice')
  } 
})

/*
Write a schedule block (identical set of instructions) for each included device (using devEUI), hour, and schedule ID. There may be a sequence number included, but this is optional; the API knows how to determine the most recent version/sequence number and create a new one.
Note that writing schedules does not automatically update the pending schedule; that is a separate command
*/
router.post('/devices/writeSchedule', function(req, res, next) 
{

  try {
    let devices = req.body.devices
    let hours = req.body.hours
    let scheduleID = req.body.scheduleID
    let instructions = req.body.instructions
    let sequence_number = req.body.sequence_number

    // we don't have access to the actual es_schedule.mjs file, but we can convert the array of instructons into one so we can use the member functions
    let base_schedule_block = es_api.convertFormOutputToScheduleBlock(instructions)
    // console.log(base_schedule_block)

    let prom  = es_api.writeScheduleBlocksToDevices(devices, hours, scheduleID, base_schedule_block, sequence_number) //
    .then(result => {
      let allValid = result.every(r => {return r === true})
      console.log('schedule write returned results: ' + JSON.stringify(result))
      res.send(allValid)
    }).catch(error => {
      console.log('error when writing schedules')
      console.error(error)
      console.log('that was the error in POST')
      res.status(500).send(error)
    })
  } catch (error) {

    console.error(error)
    console.log('error in /devices/writeSchedule')
  } 

})

/*
Update the pending schedule for a set of devices
*/
router.post('/devices/updatePending', function(req, res, next) 
{

  try {
    let devices = req.body.devices
    let scheduleID = req.body.scheduleID

    if (devices.length == 0)
    {
      res.send('No devices selected!')
      return
    }

    // the promise here is actually an aggregate of the promises for each device. Each will return true or false, depending on whether it was successful or not
    let prom  = es_api.updatePendingScheduleForDevices(devices, scheduleID) 
    .then(result => {
      let allValid = result.every(r => {return r === true})
      console.log('pending schedule update returned results: ' + JSON.stringify(result))
      res.send(allValid)
    }).catch(error => {
      console.log('error when updating pending schedules')
      console.error(error)
      console.log('that was the error in POST')
      res.status(500).send(error)
    })
  } catch (error) {

    console.error(error)
    console.log('error in /devices/updatePending')
  }

})


export default router
# EnviSense Programming Webpage and API

EnviSense 2.X devices feature firmware that allows users to download schedules of functions and commands to run on the data logger to sample sensors, send LoRaWAN uplink frames, perform LoRa Surveys, and download new sampling schedules. This page covers an API and webpage designed to provide a better interface for programming multiple EnviSense devices at a time.

Features include reading the current schedules for a set of EnviSense devices, writing new schedules, updating the current version of the schedule an EnviSense should use, and adding a new EnviSense device to Chirpstack and the scheduling backend (SQL database).

[Youtube Video demonstrating how to use the webpage](https://www.youtube.com/watch?v=iqqBHtoFtcI)

## What is the Schedule For?

The scheduler exists to remotely program EnviSense devices during deployment. LoRaWAN networks reach 5-20 km away from the gateway in rural areas and service many devices that may be difficult to reach physically. Sending a human to reprogram these should be avoided at all costs, saving time and money.

In practice, many of the low-level functionalities on EnviSense, like configuring communication buses or reading values from sensors, will not meaningfully change throughout the lifetime of a sensor. However, the quantities to be measured and how often they should be measured will change. During seasons of little environmental activity, like dry seasons and drought, sampling need not occur frequently – the environment is not changing, and measuring and uplinking data is wasteful of energy. Yet during rainy seasons, there are more changes in the environment, so we should sample more frequently. The application and physical environment change, as should an EnviSense's sampling schedule.

Our scheduler is a strategy for remote reprogramming, in which we write a schedule of commands for each hour of the day. This schedule of (up to 11) commands includes a time to run the command (in seconds relative to the top of the hour), a function to run, and an optional parameter. We fit a one-hour schedule block into a single downlink command, so updating the entire schedule on a device involves 24 downlinks over LoRaWAN, assuming no retransmissions.  

With a bit more development effort, we will be able to create web-apps that write new schedules automatically based on external data sources like meteorological predictions. Simple rules-based engines like [IFTTT](https://ifttt.com/) are likely sufficient for this, and could be used to modify the 'pendingSchedule' for devices in a particular geographic region to increase or decrease sampling rates based on imminent weather patterns. A system like this uses therefore external predictions and stimuli to modify broad-scale sensing behavior to optimize the tradeoff between data quality and battery life.  

## The API and Web-Server

The webpage is a simple HTML site with forms for selecting devices to program, creating/reading schedules, updating which schedule a set of devices should run, and registering a new EnviSense device. The API supports the backend functionality of this webpage by interacting with ChirpStack and the SQL database that stores schedules for EnviSense devices. The webpage makes HTTP requests to the web-server, which invokes the API and returns results via HTTP.

### Installation

We assume this web-server will be setup on a Linux server (expected to be running Ubuntu) with a private network connection such that it can reach whatever server/VM is hosting ChirpStack and the SQL database and cannot be reached by public clients lacking credentials within that private network. These are developed with newer versions of NodeJS in mind (should be at least Node v14.; check with ``node -v``; to upgrade, see [option 2 on this page](https://phoenixnap.com/kb/update-node-js-version)).  

To install the repository and setup the web-server, follows these instructions:  

* clone the repo into the folder of your choice: ``git clone git@bitbucket.org:ccsg-res/es-programming-api.git``
* install dependencies in the api: ``cd es-programming-api/api; npm install``
  * If this fails, check your network connectivity to public domains and the version of node and npm. To briefly test, run ``node api.mjs``
* install dependencies in the web-server: ``cd ../web-server; npm install``

### Configuring the API

We use a configuration file to provide hosts and credentials for the API and web server. An example may be found as 'config-example.json' in the 'api' directory. Make a copy of this with the command ``cp config-example.json config.json``. The API will read from config.json.

Most likely all the hosts will need to be changed, as will the database name, user, and passwords. Ports may need to change as well. For the database name, this should be the same table that includes the 'schedule', 'device', 'deployment', and 'functions' tables. The name of this database can be found by looking in the PHPMyAdmin interface for the SQL database. Note that since the passwords needs to be stored within this configuration file, this web-server and API should run on a secure machine with access to the database and ChirpStack host machine through a secure VPN. **Anybody with access to the webpage will be able to manipulate SQL and ChirpStack through this interface.**  

By default, the webpage will be accessible on this machine at port 3124, but this should be changed if another service is using this port (run ``sudo netstat -tunlp | grep 3124``, and if nothing is displayed, port 3124 is free for use).

The configuration file also sets default parameters for adding devices to Chirpstack, namely the application ID (which should match the number in the MQTT topic), the appkey (which each EnviSense should know in its firmware; 32-character hex-string), and the device profile ID (defines a few network parameters). The device profile ID is a bit difficult to find; look for the log hex-string string in the URL when viewing the device profile, as shown in the image:

![image info](./docs/images/device-profile-id.png)

Setting these defaults for ChirpStack (the appkey, deviceProfileID, and applicationID) helps the API know how to configure new devices created using the webpage.

### Running the Web-Server


Once the configuration file is setup appropriately, starting the webpage is simple; navigate to the web-server directory of the repo, and run ``npm start``, which will start the ExpressJS application using ``bin/www.js``.  
 
To access the API via the webpage interface, visit that page in an internet browser. For the configuration copied from ``config-example.json``, this would be simply ``lora-ccsg.xm:3124``  

### Using the Web-Server

To help the user, there are blue helper buttons next to most elements in the web page that will display tooltips describing what each element does. The user is suggested to read through each these before trying to use the interface.  

In general, operations like schedule-queries and schedule-writes take time to complete, and will produce a small pop-up on completion to indicate success or failure. During this time, the user should be careful not to manipulate the set of selected EnviSense devices, hours, and schedule ID during this period to ensure consistency in the requests and checks on the responses once they arrive (a spinner to indicate transaction-in-progress would be a nice addition).

The four primary actions are to query device schedules, write device schedules, update the pending schedule, and create new EnviSense devices in Chirpstack and SQL.  

1. Querying devices for schedules will also populate the set of schedule fields, and the exact device and hour to show can be selected with dropdown menus. Those menus are populated when the query completes.  
2. Writing a schedule will take all the values in the schedule fields, format them into a schedule block, and write them for all the selected devices and hours for the selected schedule ID. The versioning on each schedule block (i.e. the 'sequence number') will be updated based on what is stored in the database; users need not concern themselves with this versioning technique.  
3. Updating a pending schedule will set which schedules (among schedule ID's A, B, C, and D) will be provided when EnviSense requests current schedule information. Writing a schedule does **not** automatically do this.  
4. Creating a new device requires the user provide a device EUI and device name, where the former is a 16-character hex string unique to the device, and the latter is simply a descriptive text-string (recommended to follow some naming pattern or hierarchy). Creating this device will entail adding it to chirpstack (where the application, device profile, and app key are assumed from the config.json file we filled earlier). Then, it is added to the SQL database along with 4 default schedules for the 4 schedule IDs. By default, schedule A is active on the device, and those default schedules will sample all attached sensors and uplink results every 15 minutes, 60 minutes, 6 hours, and 15 minutes (identical to 'A'). The sampling will include all sensors (and if the SDI-12 box is checked, those will be included too).  

The web-server also populates the list EnviSense devices when the page loads with ChirpStack. It is possible for some devices to exist in ChirpStack but not the SQL database.  

### Installing the Web-Server to run at Boot

Once the server is running, we should make this so it automatically starts at boot so that no one needs to manually restart it every time the server shuts down or reboots for updates.

Previously, we have used 'forever' and 'forever-service' to run nodeJS programs in the background. Unfortunately, Javascript and nodeJS change rapidly, and the web framework, ExpressJS, used for this webserver does not play well with 'forever'. Instead, we'll use [pm2](https://pm2.keymetrics.io/), which is likely already running for node-red (we use this for pinging gateways and reporting latency to the time-series database).

There may be other pm2 services running. You should run ``sudo pm2 list`` to see what is running in case anything is removed between updates to pm2 and installing this web-server as a startup service.  

If pm2 is already installed, it may need an upgrade to support some of the features used in the webserver. Upgrade with the following command: ``npm install pm2 -g; sudo pm2 update``

* Navigate to the web-server directory and start the webserver with pm2: ``sudo pm2 start ./bin/www.js``
* Tell pm2 to run the set of saved configurations at boot: ``sudo pm2 startup``
* Save the configuration to run at startup: ``sudo pm2 startup; sudo pm2 save``

That's it! When the server reboots, the webpages should be accessible within a moment of startup.

#### Fixing Missing PM2 Services

If node-red was running, it may have been removed from the set of startup commands. During the creation of these instructions, ``sudo pm2 list`` had previously showed node-red, but did not after upgrading pm2. To solve this, node-red needs to be added again. However, it was probably running a specific 'flow', most likely located in ~/.node-red (a json file with 'flows' in the filename). To add this back:

* Run node-red again ``sudo pm2 start node-red -- YOUR_FLOWS_FILE.json``  
  * Anything after the -- is an argument for the program you run
* Save the current configuration for startup: ``sudo pm2 startup``

This same pattern can be used for anything else that was running within pm2 before the upgrade. 

### Debugging  

If things go poorly, there are a few places to look for issues. The webserver runs code in two places: in the user's browser and on the server hosting the webserver. Error and debug messages are printed to the console in each case. In the browser, this is usually within a set of 'developer tools', but depends on the exact browser (development was tested in Firefox and Chrome). In the server host, it simply goes to command line, at least if the application was run directly from the command line. If it was run with pm2, it will be under ``/home/USERNAME/.pm2/logs/www-out.log``.

Most likely, issues will be in configuration parameters (``config.json``), network connectivity, or modifying elements of the webpage during a transaction with ChirpStack or the SQL database (the latter of which can take a few moments to complete when programming many devices are once). In some cases, a device may be partially created such that it only exists in ChirpStack or the SQL database. In that scenario, some operatons, like schedule-query, will fail. The list of devices is retrieved from ChirpStack.  
